package studentdatabaseapp;

import java.util.Scanner;

public class Student {
    private String firstName;
    private String lastName;
    private String gradeYear;
    private String studentID;
    private String courses = "";
    private int tuitionBalance = 0;
    private int costOfCourse = 600;
    private static int id = 1000;

    //  Constructor: prompt usr to enr a name and year.
    public Student() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter student first name: ");
        this.firstName = sc.nextLine();
        System.out.println("Enter student last name: ");
        this.lastName = sc.nextLine();
        System.out.println("1 - Freshmen\n2- for Sophmore\n3 - Junior\n4 - Senior\nEnter class level: ");
        this.gradeYear = sc.nextLine();
        setStudentID();
    }

    // generated id
    private void setStudentID() {
        id++;
        this.studentID = gradeYear + "" + id;
    }

    // enroll in courses
    public void enroll() {
        do {
            System.out.print("Enter course to enroll (Q to quit: )");
            Scanner sc = new Scanner(System.in);
            String course = sc.next();
            if (!course.equals("Q")) {
                courses = courses + "\n  " + course;
                tuitionBalance = tuitionBalance + costOfCourse;
            } else {
                break;
            }
        } while (1 != 0);
    }

    // view balance
    public void viewBalance() {
        System.out.println("Your balance is: $" + tuitionBalance);
    }

    // Pay tuition
    public void payTuition() {
        viewBalance();
        System.out.print("Enter your payment: $");
        Scanner sc = new Scanner(System.in);
        int payment = sc.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your payment of $ " + payment);
        viewBalance();

    }

    //Show status
    public String showInfo() {
        return "Name: " + firstName + " " + lastName + "\nGrade Level: " + gradeYear +
                "\nCourses Enrolled: " + courses +
                "\nBalance: $" + tuitionBalance;
    }
}



